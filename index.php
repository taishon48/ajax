<?php
include_once('db_access.php');

$datas = execute_sql('SELECT * FROM `dates`;');
$last_id = execute_sql('SELECT MAX(`id`) AS last_id FROM `dates`;');
$last_id = $last_id[0]['last_id'];
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset="utf-8">
<script
  src="https://code.jquery.com/jquery-2.2.4.js"
  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
  crossorigin="anonymous"></script>
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
  integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
  crossorigin="anonymous"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">
<script>
$(function(){

    $('.datepicker').datepicker();

    $('.btnClone').on('click', function(){
        //$('.btnClone').clone(true).insertAfter('.btnClone');
        $(this).clone(true).insertAfter(this);
    });


    $(".save").on('click', function(){
        var id = $(this).data('id');
        $('#form1_id').val(id);
        var date = $('#date_' + id).val();
        $('#form1_date').val(date);
    });

    $(".datepicker, .date").on('change', function(){
        var id = $(this).attr('id');   // 'date_*'
        id = id.replace(/date_/g, "");
             // 置き換えたい文字列を「/」で囲み、最後に g を付加する
        $('#form1_id').val(id);
        var date = $(this).val();
        $('#form1_date').val(date);
    });

    var next_id = <?php echo ($last_id + 1); ?>;
    $(".btnAdd").on('click', function(){
        var tr = $("#copy_tr").clone(true);
        tr.attr('id', '');
        tr.find('.date').attr('id', 'date_' + next_id);
        tr.find('.date').datepicker();
        tr.find('.save').data('id', next_id);
        tr.find('.save').attr('data-id', next_id);
        tr.appendTo("#mytable");

        next_id++;
    });
});
$("#ajax").on('click',function(){
  $(".datepicker, .date").on('change', function(){ 
        //alert('Ajax通信');
        var id   = $('#form1_id').val();
        var date = $('#form1_date').val();
        $.ajax({
            url: "http://localhost/webkiso/06a/ajax.php",
            type: "POST",
            data: {id: id, date: date},
            dataType: "json"
        }).done(function(data) {

        }).fail(function(data) {
            alert("NG");
            //alert(data.status);
        });
    });
});
</script>
</head>
<body>

<table id="mytable">
    <?php foreach($datas as $data):?>
<tr>
    <td><input type="date" class="datepicker" id="date_1<?php echo $data['id']?>" value="<?php echo $data['date']?>"></td>
    <td><input type="button" class="save" data-id="<?php echo $data['id']?>" value="保存"></td>
</tr>
  <?php endforeach;?>
</table>

<table style="display:none;">
<tr id="copy_tr">
    <td><input type="text" class="date" id=""></td>
    <td><input type="button" class="save" data-id="" value="保存"></td>
</tr>
</table>

<input type="button" value="行追加" class="btnAdd">

<hr>

<form id="form1">
    <input type="text" id="form1_id" name="id" value="">
    <input type="text" id="form1_date" name="date" value="">
    <input type="button" id="ajax" name="name" value="Ajax">
</form>

</body>
</html>
